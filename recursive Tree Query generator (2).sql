

if object_id('xxx_recursive_query_generator') is null exec('create procedure xxx_recursive_query_generator as begin print 0 end')
GO
alter procedure xxx_recursive_query_generator(@RootTable sysname, @seq int output, @ParentTable sysname = null, @level int = 0, @fullPath varchar(max) = '') as
begin
    declare @indexId int
    declare @columnId int
    declare @pkname sysname
    declare @sql varchar(max)
    declare @sql1 varchar(max)
    declare @tmpTable sysname
    declare @tempFullPath varchar(max)
    set @level=@level+1
    if @level=1 begin
        set @fullPath = @rootTable
        set @columnId =1
        set @ParentTable='#tmp_1'
        set @sql=null
        select @pkname=spk.name, @indexId=i.index_id
        from sys.objects spk 
        JOIN sys.indexes i ON i.object_id=spk.parent_object_id AND i.Name=spk.Name
        where spk.parent_object_id=object_id('TRD_HEADER') and spk.type='PK'
        while INDEX_COL(@RootTable, @indexId, @columnId) is not null BEGIN
            select @sql1='declare @' + column_name + ' ' + DATA_TYPE + 
                        case DATA_TYPE WHEN 'int' THEN '' ELSE COALESCE('('+CONVERT(varchar(5), CHARACTER_MAXIMUM_LENGTH) + ')', '('+CONVERT(varchar(5), NUMERIC_PRECISION) + ',' + CONVERT(varchar(5), NUMERIC_SCALE)+')', '')
                        end
                from    information_schema.columns
                where   table_name=@rootTable
                and     column_name=INDEX_COL(@RootTable, @indexId, @columnId)
            ;
            print @sql1

            set @sql=ISNULL(@sql + ' AND ', 'select * into ' + @ParentTable + ' from ' + @RootTable + ' where ')
                + INDEX_COL(@RootTable, @indexId, @columnId) + '=@' + INDEX_COL(@RootTable, @indexId, @columnId)
            set @columnId =@columnId +1
        end
        print '--'
        print @sql
        print 'select * from ' + @ParentTable
    end 


    declare fx_csr cursor local for
        SELECT  fkName        = fk.Name
        ,       ChildTable    = OBJECT_NAME(fk.parent_object_id)
        FROM    sys.foreign_keys fk
        -- JOIN    sys.foreign_key_columns fc ON fc.constraint_object_id = fk.object_id
        where fk.referenced_object_id=object_id(@rootTable)
    declare @fkName     sysname
    ,       @ChildTable sysname
    open fx_csr
    while 1=1 begin
        fetch next from fx_csr into @fkName, @ChildTable
        if @@fetch_status!=0 break
        set @tempFullPath = @fullPath + ' ==> ' + @fkName + ' ==> ' + @ChildTable
        print 'select FullPath = ''' +  @tempFullPath + ''''

        set @tmpTable = '#tmp_' + cast(@level+1 as varchar(5)) + '_' + cast(@seq as varchar(5))
        print 'select ' + @ChildTable + '.* into ' + @tmpTable + ' from ' + @ChildTable 
        set @sql=null
        select @sql=isnull(@sql + ' AND ', ' JOIN ' + @ParentTable + ' on ')
            + ' ' + @ParentTable +'.' + COL_NAME(fc.referenced_object_id, fc.referenced_column_id) + '=' + @ChildTable + '.' + COL_NAME(fc.parent_object_id, fc.parent_column_id)
        from sys.foreign_key_columns fc 
        where fc.constraint_object_id = object_id(@fkName)
        print @sql
        print 'select * from ' + @tmpTable
        exec xxx_recursive_query_generator @ChildTable, @seq output, @tmpTable, @level, @tempFullPath

        print 'drop table    ' + @tmpTable
        set @seq = @seq + 1
   end
   close fx_csr
   deallocate fx_csr 

    if @level=1 begin
        print 'drop table ' + @ParentTable
    end

end
go


exec xxx_recursive_query_generator 'table_name',@seq=0

if object_id('xxx_recursive_query_generator') is not null drop procedure xxx_recursive_query_generator 
-- drop table primes
-- drop table factors
-- create table primes (number int, isPrime bit, constraint pk_primes primary key clustered (number, isPrime))
-- create table factors (number int, prime int, power int, constraint pk_factors primary key clustered (number, prime))

checkpoint

-- truncate table primes
-- truncate table factors

set nocount on
declare @candidate int = isnull((select max(number) from primes), 1)+1
declare @steps int = 0

while @steps<=10000000 begin

    /*
    select [existing: @candidate]=@candidate, forward=f1.number + f1.prime, f1.prime, 1+isnull(f2.power, 0), f2.power, cast(f1.number + f1.prime as decimal(18,9))/ f1.prime
    from factors f1
    left outer join factors f2 on f2.number=cast(f1.number + f1.prime as decimal(18,9)) / f1.prime and f2.prime=f1.prime
    where f1.number=@candidate
    */
    insert into factors (number, prime, power)
    select f1.number + f1.prime, f1.prime, 1+isnull(f2.power, 0)
    from factors f1
    left outer join factors f2 on f2.number=cast(f1.number + f1.prime as decimal(18,9)) / f1.prime and f2.prime=f1.prime
    where f1.number=@candidate

    if @@rowcount>0 begin
        insert into primes (number, isPrime) values (@candidate, 0)
    end
    else begin
        insert into primes (number, isPrime) values (@candidate, 1)
        insert into factors (number, prime, power)
        values  (@candidate, @candidate, 1)
        ,       (@candidate+@candidate, @candidate, iif(@candidate=2, 2, 1))
    end
    -- select [primes : @candidate]=@candidate, * from primes  
    -- select [factors: @candidate]=@candidate, * from factors order by number

    set @candidate+=1
    set @steps+=1

end

select * from primes where isprime=1
-- select * from factors 


SELECT o.name               as TableName,
    i.name                  AS IndexName,
    round(SUM(s.used_page_count) * 8 /1024.0, 3)  AS IndexSizeMB
FROM sys.dm_db_partition_stats  AS s 
JOIN sys.indexes                AS i
ON s.[object_id] = i.[object_id] AND s.index_id = i.index_id
join sys.objects                AS o
ON o.object_id = i.object_id and o.type = 'U'
-- WHERE s.[object_id] = object_id('dbo.Primes')
GROUP BY o.name, i.name
ORDER BY o.name, i.name


select  quotename(object_schema_name(fk.parent_object_id)) + '.' + quotename(object_name(fk.parent_object_id))
,       quotename(object_schema_name(fk.referenced_object_id)) + '.' + quotename(object_name(fk.referenced_object_id))
,       rootTable = case when fk.parent_object_id not in (select referenced_object_id FROM sys.foreign_keys) then 1 else 0 end
FROM    sys.foreign_keys fk

;
 SET NOCOUNT ON;
 with fkeys as
 (  
     select ChildObjectId  = fk.parent_object_id
     ,      ParentObjectId = fk.referenced_object_id
     from sys.foreign_keys fk
 )
 ,  rfk as 
 (   -- all tables not involved in an FK as child
     select ParentObjectid = null, ChildObjectId= tb.object_id
     ,      lvl = case when exists (select 1 FROM fkeys where ParentObjectId = tb.object_id) then 0 else 999 end
     from sys.tables tb
     where tb.name='TRD_HEADER' -- not exists (select 1 FROM fkeys where ChildObjectId = tb.object_id)
     union all -- all others, recursively
     select fkeys.ParentObjectid, fkeys.ChildObjectId, lvl=rfk.lvl+1
     from fkeys
     join rfk
     on   fkeys.ParentObjectid =rfk.ChildObjectId
     and  fkeys  .ParentObjectid!=fkeys.ChildObjectId
     and lvl<10
 )
 , tables as 
 (    select TableName      = quotename(object_schema_name(rfk.ParentObjectid)) + '.' + quotename(object_name(rfk.ParentObjectid))
      ,      ChildTableName = quotename(object_schema_name(rfk.ChildObjectId )) + '.' + quotename(object_name(rfk.ChildObjectId ))
      ,      lvl = lvl + case rfk.ParentObjectid when rfk.ChildObjectId then .5 else 1 end
      from rfk
) 
select * from tables
order by lvl
;

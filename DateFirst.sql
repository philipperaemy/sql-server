set datefirst 2
select  * 
,      [Week starts on Monday]=(datepart(dw, daydate)+ @@DATEFIRST-2) % 7 +1
,      [Week starts on Sunday]=(datepart(dw, daydate)+ @@DATEFIRST-1) % 7+1
,       datename(dw, daydate)
from (
    select daydate=dateadd(day, value, '20150101') 
    from dbo.IntRange(1,1, 35)
) xx

GO
if object_id('dbo.ApplyDayMask') is not null begin
    drop function dbo.ApplyDayMask
end
GO
create function dbo.ApplyDayMask(@dte Date, @dayMask int)
returns bit
as begin
    return cast(power(2, (datepart(dw, @dte)+ @@datefirst-1) % 7) as int) & @dayMask;
end;
GO

select *
, datename(dw, daydate)
, convert(binary(1), daymask)
, bin=cast(cast(dayMask &  64 as bit) as char(1))
    + cast(cast(dayMask &  32 as bit) as char(1))
    + cast(cast(dayMask &  16 as bit) as char(1))
    + cast(cast(dayMask &   8 as bit) as char(1))
    + cast(cast(dayMask &   4 as bit) as char(1))
    + cast(cast(dayMask &   2 as bit) as char(1))
    + cast(cast(dayMask &   1 as bit) as char(1))
, active=dbo.ApplyDayMask(dayDate, dayMask)
from (
    select daydate=dateadd(day, value, '20150101') 
    from dbo.IntRange(1,1, 35)
) xx
cross join
(select dayMask = value from dbo.IntRange(1,1, 127)) x
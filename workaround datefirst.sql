    set datefirst 7
declare @d datetime = '20000101'
select d, dw=datepart(dw, d), datename(dw, d), fixeddw = (datepart(dw, d) - datepart(dw, '20000101') + 13) % 7 +1
from (values ('20000101')
        ,    ('20000102')
        ,    ('20000103')
        ,    ('20000104')
        ,    ('20000105')
        ,    ('20000106')
        ,    ('20000107')
        )d(d)
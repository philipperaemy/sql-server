begin tran
go
create table testtrigger (id int identity, name varchar(10))
go
create trigger trg_testtrigger_iU on testtrigger instead of insert, update as
begin
    insert into testtrigger
    select inserted.name 
    from inserted
    left outer join deleted on inserted.id=deleted.id
    where deleted.id is null
    ;
    update upd
    set name=upd.newname
    from (select testtrigger.name, inserted.name as newname 
        from testtrigger
        join inserted
        on testtrigger.id=inserted.id
        join deleted on inserted.id=deleted.id
    ) upd
end
go


insert into testtrigger (name)
values ('un'), ('deux')
;
select * from testtrigger
;
update testtrigger set name='one' where name='un'
;
select * from testtrigger
;
GO

rollback
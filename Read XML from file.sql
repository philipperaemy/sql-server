declare @xml xml
select @xml=convert(xml, bulkcolumn)
from openrowset(bulk 'I:\databases\20140808 mssms registered servers.regsrvr', single_blob) as xx
;
select @xml
;
WITH XMLNAMESPACES(DEFAULT 'http://schemas.serviceml.org/smlif/2007/02',
                   'http://schemas.microsoft.com/sqlserver/RegisteredServers/2007/08' as RegisteredServers,
                   'http://www.w3.org/2001/XMLSchema' as xs)
-- 
select xx.node.value('.', 'varchar(50)') from @xml.nodes('/model/identity/name') xx(node)

;
WITH XMLNAMESPACES(DEFAULT 'http://schemas.serviceml.org/smlif/2007/02',
                   'http://schemas.microsoft.com/sqlserver/RegisteredServers/2007/08' as RegisteredServers,
                   'http://www.w3.org/2001/XMLSchema' as xs)
-- 
select xx.node.value('.', 'varchar(50)') from @xml.nodes('/model/xs:bufferSchema/definitions/document/docinfo/aliases/alias') xx(node)
;
declare @query varchar(255)
set @query='/model/xs:bufferSchema/definitions/document/data/xs:schema/RegisteredServers:bufferData/instances/document/data/RegisteredServers:RegisteredServer/RegisteredServers:name'
set @query='/model/xs:bufferSchema/definitions/document/docinfo/aliases/alias'
;
WITH XMLNAMESPACES(DEFAULT 'http://schemas.serviceml.org/smlif/2007/02',
                   'http://schemas.microsoft.com/sqlserver/RegisteredServers/2007/08' as RegisteredServers,
                   'http://www.w3.org/2001/XMLSchema' as xs)
-- 
select Name=xx.node.query('RegisteredServers:Name').value('.', 'varchar(50)') 
,      Name=xx.node.query('RegisteredServers:ServerName').value('.', 'varchar(50)') 
from @xml.nodes('/model/xs:bufferSchema/definitions/document/data/xs:schema/RegisteredServers:bufferData/instances/document/data/RegisteredServers:RegisteredServer') xx(node)


with TableHierarchy as (
    SELECT ParentTable=CAST('' AS sysname), fkName = CAST('' AS sysname), ChildTable=CAST('TRD_HEADER' AS sysname), ChildAlias=QUOTENAME('TRD_HEADER'), ChildObjectId=object_id('TRD_HEADER'), lvl=1, ConstraintId=-1, ParentConstraintId=-2
    UNION ALL
    SELECT  ParentTable   = CAST(OBJECT_NAME(fk.referenced_object_id) AS sysname)
    ,       fkName        = fk.Name
    ,       ChildTable    = CAST(OBJECT_NAME(fk.parent_object_id) AS sysname)
    ,       ChildAlias    = QUOTENAME(CAST(OBJECT_NAME(fk.parent_object_id) AS sysname) + '|' + fk.Name)
    ,       ChildObjectId = fk.parent_object_id
    ,       lvl           = th.lvl+1
    ,       ConstraintId  = fk.object_id
    ,       ParentConstraintId = th.ConstraintId
    FROM    TableHierarchy   th 
    JOIN    sys.foreign_keys fk on fk.referenced_object_id= th.ChildObjectId
)
,   flat as (
    SELECT TableHierarchy.* 
    ,   fkColumn=COL_NAME(fc.parent_object_id, fc.parent_column_id)
    ,   pkColumn= COL_NAME(fc.referenced_object_id, fc.referenced_column_id)
    ,   constraint_column_id
    ,   ParentAlias = (SELECT TOP 1 ChildAlias FROM TableHierarchy h2 WHERE h2.ConstraintId = TableHierarchy.ParentConstraintId)
    FROM TableHierarchy
    LEFT OUTER JOIN sys.foreign_key_columns fc ON TableHierarchy.ConstraintId = fc.constraint_object_id
)
,   reflat as (
    select * 
    ,   cChildAlias = CASE (SELECT count(distinct fkName) FROM TableHierarchy h2 where h2.ChildTable = flat.ChildTable)
                    WHEN 1 THEN ChildTable
                    ELSE ChildAlias
                    END
    ,   cParentAlias = CASE (SELECT count(distinct fkName) FROM TableHierarchy h2 where h2.ChildTable = flat.ParentTable)
                    WHEN 1 THEN ParentTable
                    ELSE ParentAlias
                    END
    from flat
)
,   rereflat as (
    select * 
    ,       tableDeclaration = CASE cChildAlias WHEN ChildTable THEN cChildAlias ELSE ChildTable + ' ' + cChildAlias END
    ,       selectItem = cChildAlias + '=''|>'', ' + cChildAlias + '.*'
    from reflat
)
,   results as (
    SELECT top 99999 * 
    ,       selects=CASE WHEN ParentTable ='' 
                THEN 'SELECT  ' + selectItem
                WHEN constraint_column_id =1
                THEN ', ' + selectItem
                ELSE ''
                END
    ,       froms=CASE WHEN ParentTable =''
                 THEN 'FROM ' + tableDeclaration
                 ELSE CASE constraint_column_id 
                    WHEN 1 THEN ' LEFT OUTER JOIN ' + tableDeclaration + ' ON ' 
                    ELSE        ' AND '
                    END + cChildAlias + '.' + fkColumn + '=' + cParentAlias + '.' + pkColumn
            END + ' '
    FROM rereflat
    ORDER BY lvl, cParentAlias, cChildAlias, constraint_column_id
)
select * from results /*

select selects from results
union all select froms from results
--*/
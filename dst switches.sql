set datefirst 7
select @@DATEFIRST
, dateadd(dd,-datepart(dw, datefromparts(datepart(yy, getutcdate()), 10, 30)) - @@datefirst % 7 + 1, datetimefromparts(datepart(yy, getutcdate()), 10, 30, 1, 0, 0, 0))
, dateadd(dd,-datepart(dw, datefromparts(datepart(yy, getutcdate()), 03, 31)) - @@datefirst % 7 + 1, datetimefromparts(datepart(yy, getutcdate()), 03, 31, 1, 0, 0, 0))
from (select dte=datefromparts(2014, 1,1)) x

select @@DATEFIRST

/* http://en.wikipedia.org/wiki/Summer_Time_in_Europe */

select  DATEFROMPARTS(year, 3, 31 - ((((5*year)/4)+4) % 7))
,       DATEFROMPARTS(year, 10, 31 - ((((5*year)/4)+1) % 7))
from (values (2010), (2011), (2012), (2013), (2014), (2015), (2016), (2017), (2018), (2019))y(year)


CREATE TYPE TimeSeries AS TABLE 
(
	id      int,
    value   float,
    primary key (id)
)
GO

create function dbo.ExponentialMovingAverage(@Series TimeSeries READONLY, @NbPoints int) 
returns @results table  (id int, value float)
as 
begin
    declare @rc int=0;
    declare @LastAverage float;
    declare @id int, @value float;
    insert into @results (id, value)
    select id, value
    from (select id, value=avg(value) over (order by id), rownum=row_number() over (order by id)
            from @Series
    ) xx
    where rownum<= @nbPoints;

    select @LastAverage = value
    from   @results
    where  id = (select max(id) from @results)

    declare csr cursor local for 
    select id, value
    from (select id, value, rownum=row_number() over (order by id)
            from @Series
    ) xx
    where rownum>@nbPoints
    order by id;

    open csr;
    while 1=1 begin
        fetch next from csr into @id, @value;
        if @@fetch_status!=0 break;
        set @LastAverage=2*(@value - @Lastaverage) / (@nbPoints+1) + @Lastaverage;
        insert into @results(id, value) values (@id, @LastAverage);
    end;
    close csr;
    deallocate csr;

    return;
    
end;
GO


declare @TimeSeries TimeSeries

WITH series as (select seq=value, price=(ABS(CAST(CAST(NEWID() AS VARBINARY) AS int)) % 20)+10, seed=(1+SIN((value*99999) % 113))*1000 from  [dbo].[IntRange] (1,1,10000)) 
insert into @TimeSeries (id, Value)
SELECT  seq, price
from series

select ts.*, ema10=ema10.value , ema20=ema20.value , ema50=ema50.value from @TimeSeries ts
join dbo.ExponentialMovingAverage(@TimeSeries, 10) ema10
on ts.id=ema10.id
join dbo.ExponentialMovingAverage(@TimeSeries, 20) ema20
on ts.id=ema20.id
join dbo.ExponentialMovingAverage(@TimeSeries, 50) ema50
on ts.id=ema50.id
;
GO

-- select rand(1), rand(2), RAND(3), RAND(4000)
drop  function dbo.ExponentialMovingAverage
drop TYPE TimeSeries 
GO
# README #

The scripts in this repository are various useful scripts running on SQL Server from version 2000 to 2012 / 2014.
They're given as-is and with no support.
The name of the script is usually good enough for understanding their purpose.


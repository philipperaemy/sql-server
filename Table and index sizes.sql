
SELECT o.name               as TableName,
    i.name                  AS IndexName,
    round(SUM(s.used_page_count) * 8 /1024.0, 3)  AS IndexSizeMB
FROM sys.dm_db_partition_stats  AS s 
JOIN sys.indexes                AS i
ON s.[object_id] = i.[object_id] AND s.index_id = i.index_id
join sys.objects                AS o
ON o.object_id = i.object_id and o.type = 'U'
-- WHERE s.[object_id] = object_id('dbo.Primes')
GROUP BY o.name, i.name
ORDER BY o.name, i.name

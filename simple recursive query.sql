
declare @loop int = 1
,       @cnt int
,       @start datetime2 = getdate()
while @loop>0 begin
    with cte as
    (   select lvl=0
        union all
        select lvl=cte.lvl+1
        from cte
        where lvl<32767
    )
    select @cnt = count(*) from cte 
    OPTION (MAXRECURSION 32767)
    ;
    set @loop-=1;
end
print datediff(ms, @start, getdate())
GO
declare @loop int = 1
,       @cnt int
,       @start datetime2 = getdate()
while @loop>0 begin
    select @cnt = count(*) from dbo.intRange(1,1,32767);
    set @loop-=1;
end
print datediff(ms, @start, getdate())
GO

with cte as
(   select lvl=1
    union all
    select lvl=cte.lvl+1
    from cte
    where lvl<100
)
select * from cte 
OPTION (MAXRECURSION 32767)
;
    
USE [master]
GO
declare @servers table(ServerAlias varchar(100), ServerName varchar(50))

declare @xml xml
select @xml=convert(xml, bulkcolumn)
from openrowset(bulk 'I:\databases\20140808 mssms registered servers.regsrvr', single_blob) as xx
;
WITH XMLNAMESPACES(DEFAULT 'http://schemas.serviceml.org/smlif/2007/02',
                   'http://schemas.microsoft.com/sqlserver/RegisteredServers/2007/08' as RegisteredServers,
                   'http://www.w3.org/2001/XMLSchema' as xs)
insert into @servers
select  ServerAlias = case 
            when ServerAlias like 'staging %' then substring(ServerAlias, 9, 100) + ' Staging'
            when ServerAlias like 'production %' then substring(ServerAlias, 12, 100) + ' Prod'
            when ServerAlias like 'dev %' then substring(ServerAlias, 5, 100) + ' Dev'
            else ServerAlias
        end
,       ServerName           
from (
    select ServerAlias=xx.node.query('RegisteredServers:Name').value('.', 'varchar(50)') 
    ,      ServerName=xx.node.query('RegisteredServers:ServerName').value('.', 'varchar(50)') 
    from @xml.nodes('/model/xs:bufferSchema/definitions/document/data/xs:schema/RegisteredServers:bufferData/instances/document/data/RegisteredServers:RegisteredServer') xx(node)
) servers
;
select * from @servers
order by ServerAlias
;



declare @sqlAdd varchar(max) = '
USE [master]
EXEC master.dbo.sp_addlinkedserver @server = N''@ServerAlias'', @srvproduct=N''sql_server'', @provider=N''SQLOLEDB'', @datasrc=N''@ServerName''
EXEC master.dbo.sp_serveroption @server=N''@ServerAlias'', @optname=N''collation compatible'', @optvalue=N''false''
EXEC master.dbo.sp_serveroption @server=N''@ServerAlias'', @optname=N''data access'', @optvalue=N''true''
EXEC master.dbo.sp_serveroption @server=N''@ServerAlias'', @optname=N''dist'', @optvalue=N''false''
EXEC master.dbo.sp_serveroption @server=N''@ServerAlias'', @optname=N''pub'', @optvalue=N''false''
EXEC master.dbo.sp_serveroption @server=N''@ServerAlias'', @optname=N''rpc'', @optvalue=N''true''
EXEC master.dbo.sp_serveroption @server=N''@ServerAlias'', @optname=N''rpc out'', @optvalue=N''true''
EXEC master.dbo.sp_serveroption @server=N''@ServerAlias'', @optname=N''sub'', @optvalue=N''false''
EXEC master.dbo.sp_serveroption @server=N''@ServerAlias'', @optname=N''connect timeout'', @optvalue=N''0''
EXEC master.dbo.sp_serveroption @server=N''@ServerAlias'', @optname=N''collation name'', @optvalue=null
EXEC master.dbo.sp_serveroption @server=N''@ServerAlias'', @optname=N''lazy schema validation'', @optvalue=N''false''
EXEC master.dbo.sp_serveroption @server=N''@ServerAlias'', @optname=N''query timeout'', @optvalue=N''0''
EXEC master.dbo.sp_serveroption @server=N''@ServerAlias'', @optname=N''use remote collation'', @optvalue=N''true''
EXEC master.dbo.sp_serveroption @server=N''@ServerAlias'', @optname=N''remote proc transaction promotion'', @optvalue=N''true''
EXEC master.dbo.sp_addlinkedsrvlogin @rmtsrvname = N''@ServerAlias'', @locallogin = NULL , @useself = N''True'', @rmtuser = N''''
GO
'

declare @sqlDrop varchar(max) = '
USE [master]
EXEC master.dbo.sp_dropserver @server=N''@srvname'', @droplogins=''droplogins''
GO
'
select replace(@sqlDrop, '@srvname', srvname)
from master.sys.sysservers
union all
select distinct replace(replace(@sqlAdd, '@servername', serverName), '@ServerAlias', ServerAlias) from @servers


/* SQL2000 :
USE [master]
GO

EXEC master.dbo.sp_dropserver @server=N'Affinity Prod kstmsdb03vs1', @droplogins='droplogins'
GO

EXEC master.dbo.sp_addlinkedserver @server = N'Affinity Prod kstmsdb03vs1', @srvproduct=N'MSDASQL', @provider=N'MSDASQL', @provstr=N'driver={sql server};server=kstmsdb03vs1;trusted_connection=true;database=kst_ent'
EXEC master.dbo.sp_addlinkedsrvlogin @rmtsrvname=N'Affinity Prod kstmsdb03vs1',@useself=N'True',@locallogin=NULL,@rmtuser=NULL,@rmtpassword=NULL
EXEC master.dbo.sp_serveroption @server=N'Affinity Prod kstmsdb03vs1', @optname=N'collation compatible', @optvalue=N'false'
EXEC master.dbo.sp_serveroption @server=N'Affinity Prod kstmsdb03vs1', @optname=N'data access', @optvalue=N'true'
EXEC master.dbo.sp_serveroption @server=N'Affinity Prod kstmsdb03vs1', @optname=N'dist', @optvalue=N'false'
EXEC master.dbo.sp_serveroption @server=N'Affinity Prod kstmsdb03vs1', @optname=N'pub', @optvalue=N'false'
EXEC master.dbo.sp_serveroption @server=N'Affinity Prod kstmsdb03vs1', @optname=N'rpc', @optvalue=N'true'
EXEC master.dbo.sp_serveroption @server=N'Affinity Prod kstmsdb03vs1', @optname=N'rpc out', @optvalue=N'true'
EXEC master.dbo.sp_serveroption @server=N'Affinity Prod kstmsdb03vs1', @optname=N'sub', @optvalue=N'false'
EXEC master.dbo.sp_serveroption @server=N'Affinity Prod kstmsdb03vs1', @optname=N'connect timeout', @optvalue=N'0'
EXEC master.dbo.sp_serveroption @server=N'Affinity Prod kstmsdb03vs1', @optname=N'collation name', @optvalue=null
EXEC master.dbo.sp_serveroption @server=N'Affinity Prod kstmsdb03vs1', @optname=N'lazy schema validation', @optvalue=N'false'
EXEC master.dbo.sp_serveroption @server=N'Affinity Prod kstmsdb03vs1', @optname=N'query timeout', @optvalue=N'0'
EXEC master.dbo.sp_serveroption @server=N'Affinity Prod kstmsdb03vs1', @optname=N'use remote collation', @optvalue=N'true'
EXEC master.dbo.sp_serveroption @server=N'Affinity Prod kstmsdb03vs1', @optname=N'remote proc transaction promotion', @optvalue=N'true'

*/
select 'drop '
    + case type
        when 'v' then 'view'
        when 'u' then 'table'
        when 'if' then 'function'
        when 'tf' then 'function'
        when 'fn' then 'function'
        when 'p' then 'procedure'
        else type
    end
    + ' ' + schema_name(uid) + '.' + name

from sysobjects where schema_name(uid) = 'prodref'
and not type in ('K')
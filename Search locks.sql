set nocount on
declare @locks table(spid smallint, dbid smallint, objid int, indId smallint, type nchar(4), Resource nvarchar(32), mode nvarchar(8), status nvarchar(5))
insert into @locks(spid, dbid, ObjId, IndId, Type, Resource, Mode, Status)
exec sp_lock


select l.spid, l.dbid, [user]=case db_name(l.dbid) when db_name() then user_name(p.uid) else convert(sysname, p.uid) end, db = db_name(l.dbid), mode
from (select distinct spid, dbid, mode from @locks )l
join sys.sysprocesses p on l.spid=p.spid
order by mode desc, [user]

select * from sys.sysprocesses where  spid >= 50 and blocked <> 0
-- select * from master..sysdatabases order by name

-- select * from sysusers order by name

select u.name, p.*
from sys.sysprocesses p
join sysusers u on p.uid=u.uid
join master..sysdatabases db on db.dbid=p.dbid and db.name=db_name()
-- spid in (140, 83, 51)

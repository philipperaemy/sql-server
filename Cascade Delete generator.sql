if object_id('dbo.xxx_cascade_delete_generator') is null exec('create procedure dbo.xxx_cascade_delete_generator as begin print 0 end')
GO
if object_id('dbo.xxx_gen_pk_columns') is null exec('create function dbo.xxx_gen_pk_columns() returns varchar as begin return null end')
GO
alter function dbo.xxx_gen_pk_columns(@TableName sysname, @format as varchar) returns varchar(max) as 
begin
    declare @pkname         sysname
    ,       @pkColumnName   sysname
    ,       @indexId        int
    ,       @columnId       int
    ,       @rc             varchar(max)
    select  @pkname         = null
    ,       @pkColumnName   = null
    ,       @indexId        = null 
    ,       @columnId       = 1
    ,       @rc             = null
    select @pkname=spk.name, @indexId=i.index_id
    from sys.objects spk
    JOIN sys.indexes i ON i.object_id=spk.parent_object_id AND i.Name=spk.Name
    where spk.parent_object_id=object_id(@TableName) and spk.type='PK'

    while INDEX_COL(@TableName, @indexId, @columnId) is not null BEGIN
        set @pkColumnName=INDEX_COL(@TableName, @indexId, @columnId)
        if @format='@' begin
            select @rc=isnull(@rc + ',       ', 'DECLARE ') + '@' + @pkColumnName + ' ' + DATA_TYPE + 
                        case DATA_TYPE 
                            WHEN 'int' THEN '' 
                            ELSE COALESCE('('+CONVERT(varchar(5), CHARACTER_MAXIMUM_LENGTH) + ')', '('+CONVERT(varchar(5), NUMERIC_PRECISION) + ',' + CONVERT(varchar(5), NUMERIC_SCALE)+')', '')
                        end + ' = NULL -- enter filter value here
'
            from    information_schema.columns
            where   table_name=@TableName
            and     column_name=@pkColumnName
            ;
        end
        else if @format='2' begin 
            set @rc=isnull(@rc + '{0}', '') + '{1}'+ @pkColumnName + '{2}' + @pkColumnName
        end
        else begin
            set @rc=isnull(@rc + ', ', '') + @pkColumnName
        end
        ;
        set @columnId=@columnId+1;
    end;
    return @rc;
end
GO
alter procedure dbo.xxx_cascade_delete_generator(@RootTable sysname, @seq int output, @ParentTable sysname = null, @level int = 0, @fullPath varchar(max) = '') as
begin
    declare @indexId int
    declare @columnId int
    declare @pkname sysname
    declare @sql varchar(max)
    declare @sql_RootPk varchar(max)
    declare @where varchar(max)
    declare @where_Filter varchar(max)
    declare @where_RootPk varchar(max)
    declare @pkColumnName sysname
    declare @columns varchar(max)
    declare @tmpTable sysname
    declare @tempFullPath varchar(max)
    set @level=@level+1

    if @level=1 begin
        set @fullPath = @rootTable
        set @ParentTable='#tmp_1'

        create table #r (id int identity (1,1), line varchar(max), lvl int null)

        insert into #r (line) values ('use ' + db_name())
        insert into #r (line) values ('go')
        insert into #r (line) values ('')

        insert into #r (line) values ('--')
        insert into #r (line) values ('begin transaction')
        insert into #r (line) values ('--')
        
    end



    set @sql_RootPk=null;
    select @pkname=spk.name, @indexId=i.index_id
    from sys.objects spk 
    JOIN sys.indexes i ON i.object_id=spk.parent_object_id AND i.Name=spk.Name
    where spk.parent_object_id=object_id(@RootTable) and spk.type='PK'
    -- print @pkname
    -- print @indexId
    -- print @columnId
    set @columnId =1
    set @sql_RootPk   = 'select ' + dbo.xxx_gen_pk_columns(@RootTable, '')
    set @where_Filter = 'where ' + replace(replace(replace(dbo.xxx_gen_pk_columns(@RootTable, '2'), '{0}', ' and '), '{1}', @RootTable + '.'), '{2}', '=@')
    set @where_RootPk = 'join ' + @ParentTable + ' on ' + replace(replace(replace(dbo.xxx_gen_pk_columns(@RootTable, '2'), '{0}', ' and '), '{1}', @RootTable + '.'), '{2}', '=' + @ParentTable + '.')

    if @level=1 begin
        
        insert into #r (line) values (dbo.xxx_gen_pk_columns(@RootTable, '@'))
        insert into #r (line) values (';')
        insert into #r (line) values (@sql_RootPk)
        insert into #r (line) values ('into ' + @ParentTable + ' from ' + @RootTable)
        insert into #r (line) values ('' + @where_Filter)
    end 
    insert into #r (line) values ('')


    declare fx_csr cursor local for
        SELECT  fkName        = fk.Name
        ,       ChildTable    = OBJECT_NAME(fk.parent_object_id)
        FROM    sys.foreign_keys fk
        -- JOIN    sys.foreign_key_columns fc ON fc.constraint_object_id = fk.object_id
        where fk.referenced_object_id=object_id(@rootTable)
    declare @fkName     sysname
    ,       @ChildTable sysname
    open fx_csr
    while 1=1 begin
        fetch next from fx_csr into @fkName, @ChildTable
        if @@fetch_status!=0 break
        set @tempFullPath = @fullPath + ' ==> ' + @fkName + ' ==> ' + @ChildTable
        -- insert into #r (line) values ('/****/select FullPath = ''' +  @tempFullPath + '''')

        set @where=null
        set @sql=null
        select @where=isnull(@where + ' and ', 'join ' + @ParentTable + ' on ')
            + ' ' + @ParentTable +'.' + COL_NAME(fc.referenced_object_id, fc.referenced_column_id) + '=' + @ChildTable + '.' + COL_NAME(fc.parent_object_id, fc.parent_column_id)
        from sys.foreign_key_columns fc 
        where fc.constraint_object_id = object_id(@fkName)
        ;
        if exists (select * 
            from  sys.foreign_keys fk
            where fk.referenced_object_id=object_id(@ChildTable)
        ) begin
            set @tmpTable = '#tmp_' + cast(@level+1 as varchar(5)) + '_' + cast(@seq as varchar(5))
            insert into #r (line) values ('select ' + dbo.xxx_gen_pk_columns(@ChildTable, '') + ' into ' + @tmpTable + ' from ' + @ChildTable )
            insert into #r (line) values (@where)
            update #r set lvl=@level where lvl is null
            exec xxx_cascade_delete_generator @ChildTable, @seq output, @tmpTable, @level, @tempFullPath

            insert into #r (line) values ('drop table    ' + @tmpTable)
        end
        insert into #r (line) values ('delete '+ @ChildTable + ' from '+ @ChildTable + ' ' + @where)
        insert into #r (line) values ('')
        set @seq = @seq + 1
    end
    close fx_csr
    deallocate fx_csr 

    if @level=1 begin
        insert into #r (line) values ('DELETE '+ @RootTable + ' from '+ @RootTable + ' ' + @where_RootPk)
        insert into #r (line) values ('drop table ' + @ParentTable)
        insert into #r (line) values ('--')
        insert into #r (line) values ('rollback')
    end
    update #r set lvl=@level where lvl is null
    if @level=1 begin
        select -- '/*' + str(id, 6) + '|' + str(lvl, 6) + ' */', 
            substring('                                                            ', 1, 4*(lvl-1)) + line 
        from #r order by id
        drop table #r
    end
end
go


begin transaction
SET NOCOUNT ON
create table parent (parentId int, parentName varchar(50), constraint pk_parent primary key clustered (parentId))
create table child1 (child1Id int, child1_parentId int, child1Name varchar(50), constraint pk_child1 primary key clustered(child1Id) )
create table child2 (child2Id int, child2_parentId int, child2Name varchar(50), constraint pk_child2 primary key clustered(child2Id) )
create table grandchild (grandchildId int, grandchild_childId int, grandchildName varchar(50), constraint pk_grandchild primary key clustered(grandchildId))
create table grandgrandchild (grandgrandchildId int, grandgrandchild_grandchildId int, grandgrandchildName varchar(50), constraint pk_grandgrandchild primary key clustered(grandgrandchildId))
alter table child1 add constraint fk_child1_parent foreign key(child1_parentId) references parent(parentId)
alter table child2 add constraint fk_child2_parent foreign key(child2_parentId) references parent(parentId)
alter table grandchild add constraint fk_grandchild_child foreign key(grandchild_childId) references child1(child1Id)
alter table grandgrandchild add constraint fk_grandgrandchild_child foreign key(grandgrandchild_grandchildId) references grandchild(grandchildId)

select dbo.xxx_gen_pk_columns('parent', '')
select dbo.xxx_gen_pk_columns('parent', '@')
select dbo.xxx_gen_pk_columns('parent', '2')

exec xxx_cascade_delete_generator 'parent',@seq=0

ROLLBACK
go

drop procedure dbo.xxx_cascade_delete_generator 
drop function  dbo.xxx_gen_pk_columns

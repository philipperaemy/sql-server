select 
    'select [function]=''' + schema_name(uid) + '.' + o.name  + ''', * from ' + schema_name(uid) + '.' + o.name + '(/*' + c0.name + '*/ default'
 +  isnull(', /*' + c1.name + '*/ default', '')
 +  isnull(', /*' + c2.name + '*/ default', '')
 +  isnull(', /*' + c3.name + '*/ default', '')
 +  isnull(', /*' + c4.name + '*/ default', '')
 +  isnull(', /*' + c5.name + '*/ default', '')
 +  isnull(', /*' + c6.name + '*/ default', '')
 +  isnull(', /*' + c7.name + '*/ default', '')
 +  isnull(', /*' + c8.name + '*/ default', '')
 +  isnull(', /*' + c9.name + '*/ default', '')
 +  ') order by 1, 2, 3, 4'--  + c0.name
 -- +  case when c1.name is not null then ', ' + c1.name else '' end
 -- +  case when c2.name is not null then ', ' + c2.name else '' end
 -- +  case when c3.name is not null then ', ' + c3.name else '' end
 -- +  case when c4.name is not null then ', ' + c4.name else '' end
 -- +  case when c5.name is not null then ', ' + c5.name else '' end    
 -- +  case when c6.name is not null then ', ' + c6.name else '' end
 -- +  case when c7.name is not null then ', ' + c7.name else '' end
 -- +  case when c8.name is not null then ', ' + c8.name else '' end
 -- +  case when c9.name is not null then ', ' + c9.name else '' end
 -- , c.*
from sysobjects o
left outer join syscolumns  c0  on c0.id=o.id and c0.name like '@%' and c0.colid=1
left outer join syscolumns  c1  on c1.id=o.id and c1.name like '@%' and c1.colid=2
left outer join syscolumns  c2  on c2.id=o.id and c2.name like '@%' and c2.colid=3
left outer join syscolumns  c3  on c3.id=o.id and c3.name like '@%' and c3.colid=4
left outer join syscolumns  c4  on c4.id=o.id and c4.name like '@%' and c4.colid=5
left outer join syscolumns  c5  on c5.id=o.id and c5.name like '@%' and c5.colid=6
left outer join syscolumns  c6  on c6.id=o.id and c6.name like '@%' and c6.colid=7
left outer join syscolumns  c7  on c7.id=o.id and c7.name like '@%' and c7.colid=8
left outer join syscolumns  c8  on c8.id=o.id and c8.name like '@%' and c8.colid=9
left outer join syscolumns  c9  on c9.id=o.id and c9.name like '@%' and c9.colid=10

where schema_name(o.uid) = 'prodref'
and not o.type in ('K', 'FN')
order by 1
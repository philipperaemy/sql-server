if object_id('dbo.rand') is null exec('create function dbo.rand() returns float as begin return null end')
if object_id('getNewID') is null exec('create view getNewID as select newid() as new_id')
go
alter view getNewID as select newid() as new_id
GO
alter function dbo.rand(@minvalue float, @maxvalue float, @grain int) 
returns float
as begin
    set @minvalue = coalesce(@minvalue,0)
    set @maxvalue = coalesce(@maxvalue, @grain, 1) 
    set @grain    = coalesce(@grain, 10000000)
    return (select isnull(@minvalue, 0) + abs(convert(int, convert(binary(16), new_id)) % @grain) * 1.0 / @grain * (@maxvalue-@minvalue) from getNewID);
end;
go


select rnd, AVG(rnd) over () 
from (
    select rnd=dbo.rand(default,default,default) from sysobjects
) r
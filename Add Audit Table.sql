/****** Object:  Table [dbo].[AuditLog]    Script Date: 8/19/2014 4:52:39 PM ******/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
IF OBJECT_ID('[dbo].[AuditLog]') IS NULL BEGIN
    CREATE TABLE [dbo].[AuditLog](
	    [TableName] [sysname] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	    [ColName] [sysname] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	    [keys] [varchar](1024) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	    [action] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	    [delValue] [sql_variant] NULL,
	    [insValue] [sql_variant] NULL,
	    [auditUser] [sysname] COLLATE SQL_Latin1_General_CP1_CI_AS DEFAULT (suser_name()),
	    [auditDate] [datetime] DEFAULT (getdate() )
    ) -- ON [Archives]
    ;
    ALTER AUTHORIZATION ON [dbo].[AuditLog] TO  SCHEMA OWNER 
    ;
    CREATE CLUSTERED INDEX [CIX_AuditLog] ON [dbo].[AuditLog]
    (
	    [TableName] ASC,
	    [ColName] ASC,
	    [auditDate] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
    ;
END

IF OBJECT_ID('dbo.FormatStringImpl') IS NULL EXEC('CREATE FUNCTION dbo.FormatStringImpl() RETURNS NVARCHAR(MAX) as begin return '''';end;')
IF OBJECT_ID('dbo.FormatString') IS NULL EXEC('CREATE FUNCTION dbo.FormatString() RETURNS NVARCHAR(MAX) as begin return '''';end;')
IF OBJECT_ID('dbo.FormatString2') IS NULL EXEC('CREATE FUNCTION dbo.FormatString2() RETURNS NVARCHAR(MAX) as begin return '''';end;')
IF OBJECT_ID('dbo.FormatString3') IS NULL EXEC('CREATE FUNCTION dbo.FormatString3() RETURNS NVARCHAR(MAX) as begin return '''';end;')
IF OBJECT_ID('dbo.FormatString4') IS NULL EXEC('CREATE FUNCTION dbo.FormatString4() RETURNS NVARCHAR(MAX) as begin return '''';end;')
IF OBJECT_ID('dbo.FormatString5') IS NULL EXEC('CREATE FUNCTION dbo.FormatString5() RETURNS NVARCHAR(MAX) as begin return '''';end;')
go
ALTER FUNCTION [dbo].[FormatStringImpl](@S NVARCHAR(MAX), @Placeholder INT, @V SQL_VARIANT) RETURNS NVARCHAR(MAX) AS BEGIN RETURN REPLACE(@S, '{'+CAST(@Placeholder AS VARCHAR)+'}', ISNULL(CONVERT(NVARCHAR(MAX), @V, 113),'`Undefined`')); END
GO
ALTER FUNCTION [dbo].[FormatString](@S NVARCHAR(MAX), @V SQL_VARIANT) RETURNS NVARCHAR(MAX) AS BEGIN RETURN dbo.FormatStringImpl(@S , 0, @V); END
GO
ALTER FUNCTION [dbo].[FormatString2](@S NVARCHAR(MAX), @V0 SQL_VARIANT, @V1 SQL_VARIANT) RETURNS NVARCHAR(MAX) AS BEGIN RETURN dbo.FormatStringImpl(dbo.FormatStringImpl(@S, 0, @V0), 1, @V1); END
GO
ALTER FUNCTION [dbo].[FormatString3](@S NVARCHAR(MAX), @V0 SQL_VARIANT, @V1 SQL_VARIANT, @V2 SQL_VARIANT) RETURNS NVARCHAR(MAX) AS BEGIN RETURN dbo.FormatStringImpl(dbo.FormatStringImpl(dbo.FormatStringImpl(@S , 0, @V0), 1, @V1), 2, @V2); END
GO
ALTER FUNCTION [dbo].[FormatString4](@S NVARCHAR(MAX), @V0 SQL_VARIANT, @V1 SQL_VARIANT, @V2 SQL_VARIANT, @V3 SQL_VARIANT) RETURNS NVARCHAR(MAX) AS BEGIN RETURN dbo.FormatStringImpl(dbo.FormatStringImpl(dbo.FormatStringImpl(dbo.FormatStringImpl(@S , 0, @V0), 1, @V1), 2, @V2), 3, @V3); END
GO
ALTER FUNCTION [dbo].[FormatString5](@S NVARCHAR(MAX), @V0 SQL_VARIANT, @V1 SQL_VARIANT, @V2 SQL_VARIANT, @V3 SQL_VARIANT, @V4 SQL_VARIANT) RETURNS NVARCHAR(MAX) AS BEGIN RETURN dbo.FormatStringImpl(dbo.FormatStringImpl(dbo.FormatStringImpl(dbo.FormatStringImpl(dbo.FormatStringImpl(@S , 0, @V0), 1, @V1), 2, @V2), 3, @V3), 3, @V4); END
GO

/****** Object:  StoredProcedure [dbo].[GenAuditTrigger]    Script Date: 8/19/2014 4:51:26 PM ******/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('GenAuditTrigger') IS NULL EXEC ('CREATE procedure [dbo].[GenAuditTrigger] AS BEGIN PRINT '''' END')
GO
ALTER procedure [dbo].[GenAuditTrigger](@TableName SYSNAME)
AS
DECLARE @Cr           CHAR(2)
DECLARE @Opening      VARCHAR(MAX)
,       @Closing      VARCHAR(MAX)
,       @Cursor       VARCHAR(MAX)
,       @Action       VARCHAR(MAX)
,       @Fetchinto    VARCHAR(MAX)
,       @Declarations VARCHAR(MAX)
,       @Inserts      VARCHAR(MAX)
,       @Columns      VARCHAR(MAX)
,       @Idx          INT
,       @Keys         VARCHAR(MAX)
,       @Join         VARCHAR(MAX)
,       @KeyName      SYSNAME
,       @KeyId        INT
,       @TriggerName  SYSNAME
;
SET     @cr           = '
'
;
SELECT  @Fetchinto    = 'OPEN csr;' + @Cr + 'WHILE 1=1 BEGIN ' + @Cr + '    FETCH NEXT FROM csr INTO @Key, @Action'
,       @Declarations = 'DECLARE @Key VARCHAR(MAX), @Action CHAR(1)'
,       @Inserts      = ''
,       @Idx          = 0
,       @TriggerName  = CASE WHEN @tableName like '%.%' 
                             THEN LEFT(@TableName, charindex('.', @TableName)) + 'TRGA_' + SUBSTRING(@TableName, charindex('.', @TableName)+1, 255) + '_IUD'
                             ELSE 'TRGA_' + @TableName + '_IUD'
                             END


SET @Opening= CASE WHEN EXISTS(SELECT * FROM sys.objects WHERE object_id=object_id(@TriggerName) AND TYPE IN('TR'))
                THEN 'ALTER' ELSE 'CREATE' END
            + ' TRIGGER ' + @TriggerName + ' ON ' + @TableName + ' AFTER UPDATE, DELETE NOT FOR REPLICATION AS BEGIN'
            +@Cr+'SET nocount on'
SET @Closing=@Cr + '    END;' + @Cr + '    CLOSE csr;' + @Cr + '    DEALLOCATE csr;' + @Cr + 'END;'
SELECT TOP 1 @KeyName=o.Name, @KeyId=i.index_id
FROM sys.objects o
JOIN sys.indexes i ON o.parent_object_id=i.object_id AND o.Name=i.Name
WHERE o.type in('UK', 'PK') 
AND parent_object_id=object_id(@TableName)
ORDER BY CASE o.type WHEN 'PK' THEN 1 ELSE 2 END

IF @KeyName IS NULL raiserror('Audit trigger generation fails. Table %s has no PK or UK.', 18, 1, @TableName);
PRINT dbo.FormatString2('Creating audit trigger on table {0} using key {1}.', @TableName, @KeyName);

WHILE 1=1 BEGIN
    SET @Idx=@Idx+1;
    print dbo.FormatString3('INDEX_COL({0},{1},{2})',@TableName, @KeyId, @Idx)
    IF INDEX_COL(@TableName, @KeyId, @Idx) IS NULL BREAK;
    PRINT dbo.FormatString3('Creating audit trigger on table {0} using key {1}. Column {2}', @TableName, @KeyName, INDEX_COL(@TableName, @KeyId, @Idx) );


    SELECT  @Cursor=ISNULL(@Cursor + '+'';''+', 'DECLARE csr CURSOR LOCAL FOR '+@Cr+'    SELECT  ') + '''"''+COALESCE(' + insName + ', ' + delName + ', ''`Null`'')+''"'''
    ,       @Action=ISNULL(@Action, '    , CASE WHEN i.' + Name + ' IS NULL THEN ''D'' WHEN d.' + Name + ' IS NULL THEN ''I'' ELSE ''U'' END')
    ,       @Join  =ISNULL(@Join + @Cr + ' AND ', ' FROM inserted i FULL OUTER JOIN deleted d ON ') + 'd.' + Name + '=i.' + Name
    FROM (
        SELECT  insName= dbo.FormatString(conversion, 'I.')
        ,       delName= dbo.FormatString(conversion, 'D.')
        ,       Name
        FROM(
            SELECT  'CONVERT(NVARCHAR(1024),{0}['+ c.Name + ']' + CASE WHEN t.Name LIKE '%DATE%' or t.Name LIKE '%TIME%' THEN ', 127)' ELSE ')' END conversion
            ,       Name='['+c.Name+']'
            FROM sys.columns c
            JOIN sys.types t ON c.system_type_id=t.system_type_id
            WHERE c.object_id=object_id(@TableName)
            AND c.Name=INDEX_COL(@TableName, @KeyId, @Idx)
        ) xx
    ) xx
END
SET @Cursor = @Cursor + @Cr + @Action;
SELECT @Cursor       = @Cursor       + @Cr + '    ,   ' + dbo.FormatString(conversion, 'd.') + ', ' + dbo.FormatString(conversion, 'i.')
,      @Declarations = @Declarations + @Cr + ',   ' + dbo.FormatString(declaration, 'D') + ', ' + dbo.FormatString(declaration, 'I')
,      @Fetchinto    = @Fetchinto    + @Cr + '   ,   @D_' + VarName + ',   @I_' + VarName 
,      @Inserts      = @Inserts
        +   @Cr + '    IF ISNULL(@D_' + VarName + ', '''')!=ISNULL(@I_' + VarName + ', '''')'
        +   @Cr + '     INSERT INTO AuditLog(TableName, ColName, keys, Action, delValue, insValue)'
        +   @Cr + '     VALUES('''+@TableName + ''', ''' + Name + ''', @Key, @Action, @D_' + VarName + ', @I_' + VarName + ');'
FROM (
        SELECT DISTINCT TOP 9999 
               conversion=CASE WHEN c.max_length >0 THEN '{0}[' + c.Name + ']' ELSE 'CONVERT(NVARCHAR(4000),{0}[' + c.Name + '])' END
        ,      c.Name
        ,      c.VarName
        ,      type=t.Name
        ,      column_id
        ,      declaration='@{0}_' + varName + ' SQL_VARIANT'
        FROM (select varName= REPLACE(REPLACE(Name, ' ', '_'), '%', 'perc'), * FROM sys.columns) c
        JOIN sys.types t ON c.system_type_id=t.system_type_id AND c.user_type_id=t.user_type_id
        WHERE c.object_id=object_id(@TableName) -- 'Calendar') -- @TableName)
        AND NOT c.Name in('AuditDate', 'AuditUser')
        AND NOT COLUMNPROPERTY(OBJECT_ID, c.name, 'IsComputed')=1
        ORDER BY column_id
    )xx

SET @Fetchinto = @Fetchinto + @Cr+';'+@Cr+'IF @@FETCH_STATUS!=0 BREAK;'
PRINT '--===================================================='
PRINT '-- @Opening      ============' ;PRINT @Opening
PRINT '-- @Declarations ============' ;PRINT @Declarations
PRINT '-- @Cursor       ============' ;PRINT @Cursor
PRINT '-- @Join         ============' ;PRINT @Join
PRINT '-- @Fetchinto    ============' ;PRINT @Fetchinto
PRINT '-- @Inserts      ============' ;PRINT @Inserts
PRINT '-- @Closing      ============' ;PRINT @Closing

EXEC(@Opening
+ @Cr + @Declarations
+ @Cr + @Cursor
+ @Cr + @Join
+ @Cr + @Fetchinto
+ @Cr + @Inserts
+ @Cr + @Closing
)
GO


-- select 'exec [dbo].[GenAuditTrigger](''' + table_name + ''')' from information_schema.tables where table_schema='dbo'
GO

if OBJECT_ID('pk_tunnel') is null begin
    alter table Tunnel add constraint pk_tunnel primary key clustered (Tunnel_id)
end
GO
if OBJECT_ID('pk_ForcedVolumes') is null begin
    alter table ForcedVolumes add constraint pk_ForcedVolumes primary key clustered (ForcedVolume_id)
END
GO
exec [dbo].[GenAuditTrigger] 'Tunnel'
exec [dbo].[GenAuditTrigger] 'MaintenancePlan'
exec [dbo].[GenAuditTrigger] 'IndexFormula'
exec [dbo].[GenAuditTrigger] 'Inj_With_Profile'
exec [dbo].[GenAuditTrigger] 'ForcedVolumes'
exec [dbo].[GenAuditTrigger] 'staging.ExcludedMatlabInputTrade'
exec [dbo].[GenAuditTrigger] 'staging.DstSwitches'
exec [dbo].[GenAuditTrigger] 'staging.StorageOptiEventDataPatches'
exec [dbo].[GenAuditTrigger] 'staging.StorageOptiTradeOverlap'
exec [dbo].[GenAuditTrigger] 'staging.RefLocationUomCurrency'

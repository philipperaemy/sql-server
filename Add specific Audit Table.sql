/****** Object:  Table [dbo].[AuditLog]    Script Date: 8/19/2014 4:52:39 PM ******/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
IF OBJECT_ID('[dbo].[AuditLog]') IS NULL BEGIN
    CREATE TABLE [dbo].[AuditLog](
	    [TableName] [sysname] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	    [ColName] [sysname] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	    [keys] [varchar](1024) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	    [action] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	    [delValue] [sql_variant] NULL,
	    [insValue] [sql_variant] NULL,
	    [auditUser] [sysname] COLLATE SQL_Latin1_General_CP1_CI_AS DEFAULT (suser_name()),
	    [auditDate] [datetime] DEFAULT (getdate() )
    ) -- ON [Archives]
    ;
    ALTER AUTHORIZATION ON [dbo].[AuditLog] TO  SCHEMA OWNER 
    ;
    CREATE CLUSTERED INDEX [CIX_AuditLog] ON [dbo].[AuditLog]
    (
	    [TableName] ASC,
	    [ColName] ASC,
	    [auditDate] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
    ;
END

IF OBJECT_ID('dbo.FormatStringImpl') IS NULL EXEC('CREATE FUNCTION dbo.FormatStringImpl() RETURNS NVARCHAR(MAX) as begin return '''';end;')
IF OBJECT_ID('dbo.FormatString') IS NULL EXEC('CREATE FUNCTION dbo.FormatString() RETURNS NVARCHAR(MAX) as begin return '''';end;')
IF OBJECT_ID('dbo.FormatString2') IS NULL EXEC('CREATE FUNCTION dbo.FormatString2() RETURNS NVARCHAR(MAX) as begin return '''';end;')
IF OBJECT_ID('dbo.FormatString3') IS NULL EXEC('CREATE FUNCTION dbo.FormatString3() RETURNS NVARCHAR(MAX) as begin return '''';end;')
IF OBJECT_ID('dbo.FormatString4') IS NULL EXEC('CREATE FUNCTION dbo.FormatString4() RETURNS NVARCHAR(MAX) as begin return '''';end;')
IF OBJECT_ID('dbo.FormatString5') IS NULL EXEC('CREATE FUNCTION dbo.FormatString5() RETURNS NVARCHAR(MAX) as begin return '''';end;')
go
ALTER FUNCTION [dbo].[FormatStringImpl](@S NVARCHAR(MAX), @Placeholder INT, @V SQL_VARIANT) RETURNS NVARCHAR(MAX) AS BEGIN RETURN REPLACE(@S, '{'+CAST(@Placeholder AS VARCHAR)+'}', ISNULL(CONVERT(NVARCHAR(MAX), @V, 113),'`Undefined`')); END
GO
ALTER FUNCTION [dbo].[FormatString](@S NVARCHAR(MAX), @V SQL_VARIANT) RETURNS NVARCHAR(MAX) AS BEGIN RETURN dbo.FormatStringImpl(@S , 0, @V); END
GO
ALTER FUNCTION [dbo].[FormatString2](@S NVARCHAR(MAX), @V0 SQL_VARIANT, @V1 SQL_VARIANT) RETURNS NVARCHAR(MAX) AS BEGIN RETURN dbo.FormatStringImpl(dbo.FormatStringImpl(@S, 0, @V0), 1, @V1); END
GO
ALTER FUNCTION [dbo].[FormatString3](@S NVARCHAR(MAX), @V0 SQL_VARIANT, @V1 SQL_VARIANT, @V2 SQL_VARIANT) RETURNS NVARCHAR(MAX) AS BEGIN RETURN dbo.FormatStringImpl(dbo.FormatStringImpl(dbo.FormatStringImpl(@S , 0, @V0), 1, @V1), 2, @V2); END
GO
ALTER FUNCTION [dbo].[FormatString4](@S NVARCHAR(MAX), @V0 SQL_VARIANT, @V1 SQL_VARIANT, @V2 SQL_VARIANT, @V3 SQL_VARIANT) RETURNS NVARCHAR(MAX) AS BEGIN RETURN dbo.FormatStringImpl(dbo.FormatStringImpl(dbo.FormatStringImpl(dbo.FormatStringImpl(@S , 0, @V0), 1, @V1), 2, @V2), 3, @V3); END
GO
ALTER FUNCTION [dbo].[FormatString5](@S NVARCHAR(MAX), @V0 SQL_VARIANT, @V1 SQL_VARIANT, @V2 SQL_VARIANT, @V3 SQL_VARIANT, @V4 SQL_VARIANT) RETURNS NVARCHAR(MAX) AS BEGIN RETURN dbo.FormatStringImpl(dbo.FormatStringImpl(dbo.FormatStringImpl(dbo.FormatStringImpl(dbo.FormatStringImpl(@S , 0, @V0), 1, @V1), 2, @V2), 3, @V3), 3, @V4); END
GO

/****** Object:  StoredProcedure [dbo].[GenAuditTrigger]    Script Date: 8/19/2014 4:51:26 PM ******/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('[GenHistoryTrigger]') IS NULL EXEC ('CREATE procedure [dbo].[GenHistoryTrigger] AS BEGIN PRINT '''' END')
GO
ALTER procedure [dbo].[GenHistoryTrigger](@TableName SYSNAME)
AS
DECLARE @Cr                char(2)
DECLARE @sql               varchar(max)
,       @Columns           varchar(max)
,       @Idx               int
,       @Keys              varchar(max)
,       @KeysFilter        varchar(max)
,       @KeyName           sysname
,       @KeyId             int
,       @SchemaName        sysname
,       @TriggerName       sysname
,       @CreateTable       varchar(max)
,       @HistoryTableName  sysname
,       @TempTableName     sysname
,       @InsertSQL         varchar(max)
,       @FQTableName      sysname
;
SET     @cr           = '
'
;


SET @Idx          = 0
SET @Keys         = null
SET @SchemaName   = CASE WHEN @tableName like '%.%' 
                         THEN LEFT(@TableName, charindex('.', @TableName)-1) 
                         ELSE 'dbo'
                    END
set @TableName    = CASE WHEN @tableName like '%.%' 
                         THEN SUBSTRING(@TableName, charindex('.', @TableName)+1, 255)
                         ELSE @TableName
                    END
SET @TriggerName  = QUOTENAME(@SchemaName) + '.' + QUOTENAME('TRGH_' + @TableName + '_IUD')
SET @HistoryTableName = QUOTENAME(@SchemaName) + '.' + QUOTENAME(@tableName + '_H')
SET @TempTableName    = QUOTENAME(@SchemaName) + '.' + QUOTENAME(@tableName + '_H_TEMP')
SET @CreateTable  ='CREATE TABLE ' + @HistoryTableName + @Cr + '(   ValidFrom datetime' + @Cr + ',   ValidEnd   datetime' + @Cr + ',   auditUser sysname DEFAULT (suser_name())'
SET @FQTableName = QUOTENAME(@SchemaName) + '.' + QUOTENAME(@tableName)

SELECT TOP 1 @KeyName=o.Name, @KeyId=i.index_id
FROM sys.objects o
JOIN sys.indexes i ON o.parent_object_id=i.object_id AND o.Name=i.Name
WHERE o.type in('UK', 'PK') 
AND parent_object_id=object_id(@FQTableName)
ORDER BY CASE o.type WHEN 'PK' THEN 1 ELSE 2 END

IF @KeyName IS NULL raiserror('Audit trigger generation fails. Table %s has no PK or UK.', 18, 1, @TableName);
PRINT dbo.FormatString2('Creating audit trigger on table {0} using key {1}.', @TableName, @KeyName);

select  @Columns   = ISNULL(@Columns + ', ', '') + quotename(COLUMN_NAME)
FROM    information_schema.columns
where   TABLE_SCHEMA=@SchemaName
and     TABLE_NAME = @TableName
order by ORDINAL_POSITION;



WHILE 1=1 BEGIN
    SET @Idx=@Idx+1;
    print dbo.FormatString3('-- INDEX_COL({0},{1},{2})',@FQTableName, @KeyId, @Idx)
    IF INDEX_COL(@FQTableName, @KeyId, @Idx) IS NULL BREAK;
    PRINT dbo.FormatString3('-- Creating history trigger on table {0} using key {1}. Column {2}', @FQTableName, @KeyName, INDEX_COL(@FQTableName, @KeyId, @Idx) );

    SELECT  @Keys=ISNULL(@Keys + ', ', '') + quotename(c.Name)
    ,       @KeysFilter = ISNULL(@KeysFilter + ' AND ', '') + 'histo.'+ quotename(c.Name) + '=DELETED.' + quotename(c.Name)
    FROM sys.columns c
    JOIN sys.types t ON c.system_type_id=t.system_type_id
    WHERE c.object_id=object_id(@FQTableName)
    AND c.Name=INDEX_COL(@FQTableName, @KeyId, @Idx)
END

BEGIN TRANSACTION
BEGIN TRY
    IF OBJECT_ID(@historyTableName) IS NOT NULL BEGIN
        IF (SELECT COUNT(*)
            FROM  information_schema.columns t
            ,     information_schema.columns h 
            where t.Column_Name = h.Column_Name
            and   t.TABLE_SCHEMA = @SchemaName and   t.TABLE_NAME = @TableName
            and   h.TABLE_SCHEMA = @SchemaName and   h.TABLE_NAME = @TableName + '_H'
            and   t.COLUMN_NAME  = h.COLUMN_NAME
            and   t.DATA_TYPE    = h.DATA_TYPE
            and   ISNULL(t.CHARACTER_MAXIMUM_LENGTH, 0)=ISNULL(h.CHARACTER_MAXIMUM_LENGTH, 0)
            and   ISNULL(t.NUMERIC_PRECISION, 0)       =ISNULL(h.NUMERIC_PRECISION, 0)
            and   ISNULL(t.NUMERIC_SCALE, 0)           =ISNULL(h.NUMERIC_SCALE, 0)
            )
        <>(SELECT COUNT(*)
            FROM information_schema.columns t
            where t.TABLE_SCHEMA = @SchemaName and   t.TABLE_NAME = @TableName
           ) BEGIN -- need to recreate the table
            exec sp_rename @HistoryTableName, @TempTableName
        END
    END
    IF OBJECT_ID(@historyTableName) IS NULL BEGIN -- need to retest because we might have renamed the table there above...
        select  @CreateTable = @CreateTable + @cr + ',   '+ quotename(COLUMN_NAME) + ' ' + data_type
        FROM (
            select  top 99999
                    TABLE_SCHEMA, TABLE_NAME, COLUMN_NAME, DATA_TYPE=DATA_TYPE + 
                        case DATA_TYPE WHEN 'int' THEN ''
                            else COALESCE('('+CONVERT(varchar(5), CHARACTER_MAXIMUM_LENGTH) + ')', '('+CONVERT(varchar(5), NUMERIC_PRECISION) + ',' + CONVERT(varchar(5), NUMERIC_SCALE)+')', '')
                            end
            from    information_schema.columns
            where   TABLE_SCHEMA=@SchemaName
            and     TABLE_NAME = @TableName
            order by ORDINAL_POSITION
        )xx;
        print @CreateTable
        set @CreateTable = @CreateTable + @cr + ',   CONSTRAINT ' + QUOTENAME('PK_' + @tableName + '_H') + ' PRIMARY KEY (' + @keys + ', ValidFrom, ValidEnd)'
                            + @cr + ')'

        print @CreateTable
        exec (@CreateTable)
        print 'History table ' + @historyTableName + ' created successfully.'
        if object_id(@TempTableName) IS NOT NULL BEGIN
            set   @InsertSQL = 'INSERT INTO ' + @HistoryTableName + '(' + @Columns + ') SELECT ' + @Columns + ' FROM ' + @TempTableName
            exec (@insertSQL)
            exec ('drop table ' + @TempTableName)
        end
    end 
    print 'History table ' + @historyTableName + ' is valid.'
    commit
end try
begin catch
    rollback;
    print 'History table ' + @historyTableName + ' is NOT valid.';
    declare @errmsg varchar(512)
    SET @errmsg = convert(varchar(10), @@error) + ' - ' + ERROR_MESSAGE();
    RAISERROR(@errmsg, 18,1)
end catch

SET @sql = CASE WHEN EXISTS(SELECT * FROM sys.objects WHERE object_id=object_id(@TriggerName) AND TYPE IN('TR'))
                THEN 'ALTER' ELSE 'CREATE' END
            + ' TRIGGER ' + @TriggerName + ' ON ' + @FQTableName + ' AFTER INSERT, UPDATE, DELETE NOT FOR REPLICATION AS BEGIN'
            + @Cr + 'SET nocount on'
            + @Cr + 'DECLARE @now DATETIME SET @now=GETDATE()'
            + @Cr + 'UPDATE  histo'
            + @Cr + 'SET     ValidEnd = @now'
            + @Cr + 'FROM ' + @HistoryTableName + ' histo'
            + @Cr + 'JOIN deleted'
            + @Cr + 'ON   ' + REPLACE (@KeysFilter, ' AND ', @Cr + 'AND     ')
            + @Cr + 'WHERE ValidEnd = ''2050-12-31'''
            + @Cr + ';'
            + @Cr + 'INSERT INTO ' + @HistoryTableName 
            + @cr + '(       ' + REPLACE(@Columns+ ', ValidFrom, ValidEnd' , ', ', @cr + ',       ') 
            + @Cr + ')'
            + @Cr + 'SELECT ' + REPLACE(@Columns + ', @now, ''2050-12-31''', ', ', @cr + ',       ')
            + @Cr + 'FROM INSERTED'
            + @Cr + ';'
            + @Cr + 'END;'
PRINT @Sql
EXEC(@Sql)
GO

BEGIN TRAN
    IF OBJECT_ID('Tunnel_H'                              ) IS NOT NULL DROP TABLE Tunnel_H
    IF OBJECT_ID('MaintenancePlan_H'                     ) IS NOT NULL DROP TABLE MaintenancePlan_H
    IF OBJECT_ID('IndexFormula_H'                        ) IS NOT NULL DROP TABLE IndexFormula_H
    IF OBJECT_ID('Inj_With_Profile_H'                    ) IS NOT NULL DROP TABLE Inj_With_Profile_H
    IF OBJECT_ID('ForcedVolumes_H'                       ) IS NOT NULL DROP TABLE ForcedVolumes_H
    IF OBJECT_ID('staging.ExcludedMatlabInputTrade_H'    ) IS NOT NULL DROP TABLE staging.ExcludedMatlabInputTrade_H
    IF OBJECT_ID('staging.DstSwitches_H'                 ) IS NOT NULL DROP TABLE staging.DstSwitches_H
    IF OBJECT_ID('staging.StorageOptiEventDataPatches_H' ) IS NOT NULL DROP TABLE staging.StorageOptiEventDataPatches_H
    IF OBJECT_ID('staging.StorageOptiTradeOverlap_H'     ) IS NOT NULL DROP TABLE staging.StorageOptiTradeOverlap_H
    IF OBJECT_ID('staging.RefLocationUomCurrency_H'      ) IS NOT NULL DROP TABLE staging.RefLocationUomCurrency_H

    exec [dbo].[GenHistoryTrigger] 'Tunnel'
    exec [dbo].[GenHistoryTrigger] 'MaintenancePlan'
    exec [dbo].[GenHistoryTrigger] 'IndexFormula'
    exec [dbo].[GenHistoryTrigger] 'Inj_With_Profile'
    exec [dbo].[GenHistoryTrigger] 'ForcedVolumes'
    exec [dbo].[GenHistoryTrigger] 'staging.ExcludedMatlabInputTrade'
    exec [dbo].[GenHistoryTrigger] 'staging.DstSwitches'
    exec [dbo].[GenHistoryTrigger] 'staging.StorageOptiEventDataPatches'
    exec [dbo].[GenHistoryTrigger] 'staging.StorageOptiTradeOverlap'
    exec [dbo].[GenHistoryTrigger] 'staging.RefLocationUomCurrency'

    UPDATE Tunnel                              SET StartDate=StartDate
    UPDATE MaintenancePlan                     SET Start_Date=Start_Date
    UPDATE IndexFormula                        SET TradeNum=TradeNum
    UPDATE Inj_With_Profile                    SET Limit_Low=Limit_Low
    UPDATE ForcedVolumes                       SET StartDate=StartDate
    UPDATE staging.ExcludedMatlabInputTrade    SET TradeNum=TradeNum
    UPDATE staging.DstSwitches                 SET timezone=timezone
    UPDATE staging.StorageOptiEventDataPatches SET Action=Action
    UPDATE staging.StorageOptiTradeOverlap     SET tradeNumOverlapFormer=tradeNumOverlapFormer
    UPDATE staging.RefLocationUomCurrency      SET location_num=location_num

    UPDATE Tunnel_h                              SET ValidFrom='1900-01-01'
    UPDATE MaintenancePlan_h                     SET ValidFrom='1900-01-01'
    UPDATE IndexFormula_h                        SET ValidFrom='1900-01-01'
    UPDATE Inj_With_Profile_h                    SET ValidFrom='1900-01-01'
    UPDATE ForcedVolumes_h                       SET ValidFrom='1900-01-01'
    UPDATE staging.ExcludedMatlabInputTrade_h    SET ValidFrom='1900-01-01'
    UPDATE staging.DstSwitches_h                 SET ValidFrom='1900-01-01'
    UPDATE staging.StorageOptiEventDataPatches_h SET ValidFrom='1900-01-01'
    UPDATE staging.StorageOptiTradeOverlap_h     SET ValidFrom='1900-01-01'
    UPDATE staging.RefLocationUomCurrency_h      SET ValidFrom='1900-01-01'

COMMIT

-- begin tran 
-- delete tunnel where tunnel_id=4
-- select * from Tunnel_h where tunnel_id=4
-- rollback
-- select * from tunnel_h


-- http://www.exacthelp.com/2014/04/xquery-query-name-does-not-denote.html

DECLARE @s AS XML = '
      <P:Root xmlns:P="http://www.exacthelp.com">
            <P:Student Name="Scott"/>
            <P:Student Name="Greg"/>
      </P:Root>'
;
SELECT @s.query
      ('declare default element namespace "http://www.exacthelp.com";
       /Root/Student')

;
SELECT @s.query
      ('declare namespace P = "http://www.exacthelp.com";
       /P:Root/P:Student')

;
WITH XMLNAMESPACES(DEFAULT 'http://www.exacthelp.com')    
SELECT @s.query('/Root/Student')

;
WITH XMLNAMESPACES('http://www.exacthelp.com' AS P) 
SELECT @s.query('/P:Root/P:Student')
;
WITH XMLNAMESPACES('http://www.exacthelp.com' AS P) 
SELECT name=x.n.value('@Name', 'varchar(50)') from @s.nodes('/P:Root/P:Student') x(n)
;

select iif(1<2, 1, 2)
